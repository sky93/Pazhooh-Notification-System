unit unit_settings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.StdCtrls,
  Vcl.Imaging.GIFImg, Vcl.ExtCtrls, Vcl.ImgList, inifiles, functions, ShellAPI;

type
  Tfrm_settings = class(TForm)
    Image1: TImage;
    Image2: TImage;
    lb1: TLabel;
    Image3: TImage;
    ImageList1: TImageList;
    Button1: TButton;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    lb_pre: TLabel;
    btn_logout: TButton;
    Label3: TLabel;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn_logoutClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_settings: Tfrm_settings;
  log : boolean = false;

implementation
uses
unit_main;

{$R *.dfm}
procedure RestartThisApp;
begin
  ShellExecute(frm_main.Handle, nil, PChar(Application.ExeName), nil, nil, SW_SHOWNORMAL);
  Application.Terminate; // or, if this is the main form, simply Close;
end;

procedure Tfrm_settings.Button1Click(Sender: TObject);
var
  ini: TIniFile;

begin
  ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'settings.ini');
  try
    ini.WriteString('server','link',edit1.Text);
    ini.WriteString('settings','site',edit2.Text);
  finally
    ini.Free;
  end;
  if log = true then RestartThisApp;
  self.Close;
end;

procedure Tfrm_settings.btn_logoutClick(Sender: TObject);
var
  ini: TIniFile;

begin
log := true;
  ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'settings.ini');
  try
    ini.EraseSection('user');
  finally
    ini.Free;
  end;
end;

procedure Tfrm_settings.Edit1Change(Sender: TObject);
begin
  lb_pre.Caption := StringReplace(Edit1.text, '@', 'B3C1C7A', [rfReplaceAll, rfIgnoreCase]);
end;



procedure Tfrm_settings.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if log = true then RestartThisApp;
end;

procedure Tfrm_settings.FormCreate(Sender: TObject);
const
  font = 'BECCA';
var
  ini: TIniFile;
  link : string;
begin
  LoadResourceFont('font_BECCA','BECCA');
  self.Font.Name := font;

  ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'settings.ini');
  try
    link := ini.ReadString('server','link','');
    edit2.Text := ini.ReadString('settings','site','');
  finally
    ini.Free;
  end;

  if link = '' then
      edit1.Text := 'http://samen.irimc.org:81/user/@'
    else
      edit1.Text := link;

  Edit1Change(nil);
end;


end.
