﻿unit unit_main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls, Vcl.Imaging.GIFImg, functions, Vcl.ImgList, Vcl.ComCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, inifiles;

type
  Tfrm_main = class(TForm)
    btn_barcode: TButton;
    img_cover: TImage;
    lb_sitlink: TLabel;
    edit_barcode: TEdit;
    ImageList1: TImageList;
    Panel1: TPanel;
    Button1: TButton;
    lb_stat: TLabel;
    IdHTTP1: TIdHTTP;
    btn_update: TButton;
    TrayIcon1: TTrayIcon;
    Button3: TButton;
    lb_username: TLabel;
    btn_settings: TButton;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    lb_site: TLabel;
    Image4: TImage;
    Image5: TImage;
    procedure img_coverMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lb_sitlinkClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_barcodeClick(Sender: TObject);
    procedure edit_barcodeKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure btn_updateClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btn_settingsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  type
  Server2 = class(TThread)
  protected
    procedure Execute; override;
  end;

  type
  version = class(TThread)
  protected
    procedure Execute; override;
  end;


var
  frm_main           : Tfrm_main;
  s2                 : TThread  = nil;
  th_version         : TThread  = nil;
  username, password : string;
  llink : string = 'http://samen.irimc.org:81/user/@';

implementation

uses
  unit_user, unit_settings, SplashForm;

{$R *.dfm}

procedure version.Execute;
var
  server  : TIdHttp;
  Parameters: TStringList;
  Response: TStringStream;
  Current_Version, Last_Version : double;
  outt : Boolean;
  buttonSelected : Integer;
  Link : string;
  temp : string;
begin
  outt := false;
  Synchronize(procedure begin
    frm_main.lb_stat.Caption := 'درحال ارتباط با سامانه...';
  end);
 // try
    response := TStringStream.Create;
    Parameters := TStringList.Create;
    server :=  TIdHttp.Create();
    Parameters.Add('BECCA=4 EVA!');
    Parameters.Add('LASTVERSION=B');
    try
      Server.Post('http://pazhooh.com/win/p.php',Parameters,response);
    except
      Synchronize(procedure begin
        frm_main.lb_stat.Caption := '';    //error message
      end);
    end;

    if (Response.DataString <> '') AND (pos('|',Response.DataString) > 0) then
      begin
        Link := Copy(Response.DataString, pos('|',Response.DataString) + 1, length(Response.DataString));

        Current_Version := StrToFloat(GetAppVersionStr);
        temp            := Copy(Response.DataString, 1 , pos('|',Response.DataString) - 1);
        Last_Version    := StrToFloat(temp);
        if Last_Version > Current_Version then
          begin
           Synchronize(procedure begin
              buttonSelected := MessageDlg('نسخه ' + floattostr(Last_Version) + ' موجود میباشد. آیا مایل به آپدیت نرم افزار هستید؟',mtInformation, [mbYes,mbNo], 0);
              if buttonSelected = mrYes then ShellOpen(Link);

            end);
          end;
      end
      else
        begin
          Synchronize(procedure begin
            frm_main.lb_stat.Caption := '';    //error message
            outt := true;
          end);
        end;
      if outt then Exit;



    Synchronize(procedure begin
      //showmessage(Response.DataString);
      frm_main.lb_stat.Caption := '';
    end);
//  except
//    Synchronize(procedure begin
//      showmessage(Response.DataString);
//      frm_main.lb_stat.Caption := 'خطا!';
//    end);
end;


procedure Server2.Execute;
var
  barcode : string;
  server  : TIdHttp;
  Parameters: TStringList;
  Response: TStringStream;
begin
  Synchronize(procedure begin
    barcode := frm_main.edit_barcode.Text;
  end);
     Synchronize(procedure begin
    frm_main.lb_stat.Caption := 'درحال ارتباط با سامانه...';
   end);
   try
  response := TStringStream.Create;
  Parameters := TStringList.Create;
  server :=  TIdHttp.Create();
  Parameters.Add('BECCA=4 EVA!');
  Parameters.Add('BARCODE=' + barcode);
  Server.Post('http://pazhooh.com/win/p.php',Parameters,response);
   Synchronize(procedure begin
    showmessage(Response.DataString);
    frm_main.lb_stat.Caption := '';
   end);
   except
        Synchronize(procedure begin
    showmessage(Response.DataString);
    frm_main.lb_stat.Caption := 'خطا!';
   end);
   end;

end;


procedure Tfrm_main.btn_barcodeClick(Sender: TObject);
var
  ini: TIniFile;
begin
  ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'settings.ini');
  try
    llink := ini.ReadString('server','link','http://samen.irimc.org:81/user/@');
  finally
    ini.Free;
  end;
  edit_barcode.Visible := true;
  edit_barcode.SetFocus;
  lb_stat.Caption := 'لطفا دستگاه بارکدخوان  را در مقابل بارکد قرار دهید...';

end;

procedure Tfrm_main.btn_updateClick(Sender: TObject);
begin
  th_version := version.Create(False);
end;

procedure Tfrm_main.Button1Click(Sender: TObject);
begin
beep;
end;

procedure Tfrm_main.Button3Click(Sender: TObject);
begin
TrayIcon1.ShowBalloonHint;
//showmessage(password);
end;

procedure Tfrm_main.btn_settingsClick(Sender: TObject);
var
  frm_settings: Tfrm_settings;
begin
  frm_settings := Tfrm_settings.Create(Application);
  frm_settings.ShowModal;
end;

procedure Tfrm_main.edit_barcodeKeyPress(Sender: TObject; var Key: Char);
var
tlink : string;
begin

if ord(Key) = VK_RETURN then
  begin
    Key := #0; // prevent beeping
    if (IsHex(edit_barcode.text)) AND (length(edit_barcode.text) = 24) then
      begin
        tlink := llink;
        tlink := StringReplace(tlink, '@', edit_barcode.text, [rfReplaceAll, rfIgnoreCase]);
        ShellOpen(tlink);
        //s2 := server2.Create(False);
      end
    else
      begin
        lb_stat.Caption := 'لطفا یک بارکد صحیح در مقابل دستگاه قرار دهید.';
      end;
    btn_barcode.SetFocus;
  end;
end;

procedure Tfrm_main.FormCreate(Sender: TObject);
const
  font = 'BECCA';
var
  ini: TIniFile;
  u, p,site : string;
  frm_user: Tfrm_user;
  frm_splash: TFormSplash;
begin
  LoadResourceFont('font_BECCA','BECCA');
  lb_stat.Font.Color := rgb(236,22,92);
  frm_main.Font.Name := font;

  ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'settings.ini');
  try
    u := ini.ReadString('user','username','');
    p := ini.ReadString('user','password','');
    site := ini.ReadString('settings','site','');
  finally
    ini.Free;
  end;
   frm_splash := TFormSplash.Create(Application);
      frm_splash.ShowModal;
  if (u = '') or (p = '') then
    begin
      frm_user := Tfrm_user.Create(Application);
      frm_user.ShowModal;
    end
    else
    begin
      username := u;
      password := p;
    end;
  lb_username.Caption := 'نام کاربری: ' +' '+ username;
  lb_site.Caption :=  site;

  frm_main.Caption := frm_main.Caption + ' نسخه ' + GetAppVersionStr;
  btn_updateClick(nil);

end;

procedure Tfrm_main.img_coverMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  SendMessage(frm_main.Handle, WM_SYSCOMMAND, 61458, 0) ;
end;

procedure Tfrm_main.lb_sitlinkClick(Sender: TObject);
begin
    ShellOpen('http://pazhooh.com');
end;

end.
