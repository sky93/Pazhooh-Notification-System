﻿unit functions;

interface
  uses
    Vcl.StdCtrls, strutils,extctrls,system.Classes,Vcl.Imaging.GIFImg,Winapi.Windows,System.SysUtils,System.Variants,ShellAPI;
  function  shDate(DateAndTime:String):string;
  function  GetAppVersionStr: string;
  procedure ShellOpen(const Url: string; const Params: string = '');
  function  MiladyToShamsi(DTime:tdateTime):tdateTime;
  function LoadResourceFont( const ResourceName, FontName : string ):boolean;
  function IsHex(s: string): boolean;


implementation
 var
   CurDay,CurMonth : word;
   Farday          : byte;
   addFarday       : boolean;

function IsHex(s: string): boolean;
var
  i: integer;
begin
  Result := True;
  s := AnsiUpperCase(s);
  for i := 1 to length(s) do
    if not (char(s[i]) in ['0'..'9']) and not (char(s[i]) in ['A'..'F']) then
    begin
      Result := False;
      exit;
    end;
end;

function LoadResourceFont( const ResourceName, FontName : string ) : boolean;
var
   ResStream : tResourceStream;
   FontsCount : integer;
   hFont : tHandle;
begin
   ResStream := tResourceStream.Create(hInstance, ResourceName, RT_RCDATA);
   hFont := AddFontMemResourceEx(ResStream.Memory, ResStream.Size, nil, @FontsCount);
   result := (hFont <> 0);
   ResStream.Free();
end;


Function  MiladyToShamsi(DTime:tdateTime):tdateTime;
var
YearEqual:array[0..2,0..2] of integer;
AddOneDay:boolean;
AddTodays:byte;
ThisDay:word;
ThisMonth:word;
ThisYear:word;
YearDif1:Integer;
YearDif2:Integer;
TestRange1,testRange2,
FarsiRange1,FarsiRange2:Integer;
CurM,CurD:string;
Counter:Integer;

CurYear:word;
Begin
CurM:='';
CurD:='';
YearEqual[1,1]:=1997;
YearEqual[1,2]:=1998;
YearEqual[2,1]:=1376;
YearEqual[2,2]:=1377;
DecodeDate(DTime,thisyear,thismonth,thisday);
YearDif1:=ThisYear-1997;
YearDif2:=ThisYear-1998;
Testrange1:=1996-(100*4);
Testrange2:=1996+(100*4);
FarsiRange1:=1375-(100*4);
FarsiRange2:=1375+(100*4);
AddOneDay:=false;
//Rem------------------------------
Counter:=TestRange1;
while TestRange2>=Counter do begin /////////////////////////
If thisYear=counter then begin
AddOneDay:=true;
break;
end;
If counter=TestRange2 then break;
counter:=counter+4
end;//while

If AddOneDay then
addtodays:=1
Else
AddTodays:=0;
//Rem---------------------------------/////////////////
If (((ThisMonth = 3) and (thisday<(20+AddtoDays)))
or ( ThisMonth<3)) then
YearDif1:=yearDif1-1;
//Rem------------
If (((thisYear mod 2)<>0) and
(((thismonth=3) and (thisday>(20-addTodays)))
or (thisMonth>4))) then
CurYear:=yearEqual[2,1]+YearDif1
else begin
CurYear:=YearEqual[2,1]+YearDif2;
Counter:=FarsiRange1;
while counter>FarsiRange2 do begin /////////////////////////
If CurYear=Counter then begin
AddFarDay:=true;
break;
end;
end;//while

If AddFarDay then
FarDay:=1 else Farday:=0;

If (((thismonth=3) and (thisday>20-(addToDays)+FarDay)) or (thismonth>3) ) then
CurYear:=CurYear+1;
end; //First If

If AddtoDays=1 then FarDay:=0;
//Rem---------------------------------
Case thismonth of
1:Begin
If thisday<(21-Farday) then begin
CurMonth:=10;
CurDay:=(ThisDay+10)+FarDay;
end else begin
CurMonth:=11;
CurDay:=(ThisDay-20)+FarDay;
end;
end;

2:Begin
If thisday<(20-Farday) then begin
CurMonth:=11;
CurDay:=(ThisDay+11)+FarDay;
end else begin
CurMonth:=12;
CurDay:=(ThisDay-19)+FarDay;
end;
End;

3:Begin
If thisday<(21-AddToDays) then begin
CurMonth:=12;
CurDay:=(ThisDay+9)+AddToDays+FarDay;
end else begin
CurMonth:=1;
CurDay:=(ThisDay-20)+AddToDays;
end;
End;

4:Begin
If thisday<(21-AddToDays) then begin
CurMonth:=1;
CurDay:=(ThisDay+11)+AddToDays;
end else begin
CurMonth:=2;
CurDay:=(ThisDay-20)+AddToDays;
end;
End;

5:Begin
If thisday<(22-AddToDays) then begin
CurMonth:=2;
CurDay:=(ThisDay+10)+AddToDays;
end else begin
CurMonth:=3;
CurDay:=(ThisDay-21)+AddToDays;
end;
End;

6:Begin
If thisday<(22-AddToDays) then begin
CurMonth:=3;
CurDay:=(ThisDay+10)+AddToDays;
end else begin
CurMonth:=4;
CurDay:=(ThisDay-21)+AddToDays;
end;
End;

7:Begin
If thisday<(23-AddToDays) then begin
CurMonth:=4;
CurDay:=(ThisDay+9)+AddToDays;
end else begin
CurMonth:=5;
CurDay:=(ThisDay-22)+AddToDays;
end;
End;

8:Begin
If thisday<(23-AddToDays) then begin
CurMonth:=5;
CurDay:=(ThisDay+9)+AddToDays;
end else begin
CurMonth:=6;
CurDay:=(ThisDay-22)+AddToDays;
end;
End;

9:Begin
If thisday<(23-AddToDays) then begin
CurMonth:=6;
CurDay:=(ThisDay+9)+AddToDays;
end else begin
CurMonth:=7;
CurDay:=(ThisDay-22)+AddToDays;
end;
End;

10:Begin
If thisday<(23-AddToDays) then begin
CurMonth:=7;
CurDay:=(ThisDay+8)+AddToDays;
end else begin
CurMonth:=8;
CurDay:=(ThisDay-22)+AddToDays;
end;
End;

11:Begin
If thisday<(22-AddToDays) then begin
CurMonth:=8;
CurDay:=(ThisDay+9)+AddToDays;
end else begin
CurMonth:=9;
CurDay:=(ThisDay-21)+AddToDays;
end;
End;

12:Begin
If thisday<(22-AddToDays) then begin
CurMonth:=9;
CurDay:=(ThisDay+9)+AddToDays;
end else begin
CurMonth:=10;
CurDay:=(ThisDay-21)+AddToDays;
end;
End;

end;//case
//Rem-----------------
CurM:=Trim(IntTostr(CurMonth));
CurD:=Trim(IntTostr(CurDay));
//Rem-----------------
If CurMonth<10 then
CurM:='0'+Trim(IntToStr(CurMonth));
If CurDay<10 then
CurD:='0'+Trim(IntTostr(CurDay));
Result:=EncodeDate(CurYear,CurMonth,CurDay);
End;
Function  shDate(DateAndTime:String):string;
var
  Date : TDateTime;
  d,m,y : word;
  month,y2 : string;
  time : string;
  h,mi : word;
begin

  try

  Date := VarToDateTime(DateAndTime);

  time := timetostr(date);
  decodetime(date,h,mi,d,m);
  Date := MiladyToShamsi(Date);
  DecodeDate(date,y,m,d);
  mi := mi + 30;
  if mi > 59 then
    begin
      mi := mi - 60;
      inc(h);
    end;

  h := h + 3;

  if h > 23 then
    begin
      h := h - 24;
      inc(d);
    end;

  if (d > 30) and (m > 6) then
    begin
      d := 1;
      inc(m);
    end;

  if (d > 31) and (m <= 6) then
    begin
      d := 1;
      inc(m)
    end;

  if m > 12 then
    begin
      m := 0;
      inc(y);
    end;

  case m of
  1  : month := 'فروردین';
  2  : month := 'اردیبهشت';
  3  : month := 'خرداد';
  4  : month := 'تیر';
  5  : month := 'مرداد';
  6  : month := 'شهریور';
  7  : month := 'مهر';
  8  : month := 'آبان';
  9  : month := 'آذر';
  10 : month := 'دی';
  11 : month := 'بهمن';
  12 : month := 'اسفند';
  end;
  y2 := rightstr(inttostr(y),2);
  result := 'تاریخ: ' + inttostr(d) + ' ' + month + ' ' + y2 + ' در ساعت '+ inttostr(h) + ':' + inttostr(mi);
  except
     result := '';
  end;
end;
function GetAppVersionStr: string;
var
   Rec: LongRec;
begin
   Rec := LongRec(GetFileVersion(ParamStr(0)));
   Result := Format('%d.%d', [Rec.Hi, Rec.Lo]);
end;

procedure ShellOpen(const Url: string; const Params: string = '');
begin
    ShellAPI.ShellExecute(0, 'Open', PChar(Url), PChar(Params), nil, SW_SHOWNORMAL);
end;

end.
