﻿unit unit_user;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, functions, Vcl.ImgList,
  Vcl.Buttons, Vcl.Imaging.pngimage, Vcl.Imaging.GIFImg, Vcl.ExtCtrls, inifiles;

type
  Tfrm_user = class(TForm)
    Button1: TButton;
    ImageList1: TImageList;
    Image1: TImage;
    Image2: TImage;
    lb1: TLabel;
    Image3: TImage;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edit_username: TEdit;
    edit_password: TEdit;
    Label2: TLabel;
    Timer1: TTimer;
    lb_message: TLabel;
    ch1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  entred : boolean = false;
implementation

uses
  unit_main;

{$R *.dfm}

procedure Tfrm_user.Button1Click(Sender: TObject);
var
  ini: TIniFile;

begin
  if (edit_username.Text = '') or (edit_password.text = '') then
    begin
      lb_message.font.color := RGB(255, 73, 107);
      lb_message.Caption := 'نام کاربری یا رمز عبور صحیح نیست.';
      exit;
    end;

  ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'settings.ini');
  if ch1.Checked = true then
    begin
      ini.WriteString('user','username',edit_username.Text);
      ini.WriteString('user','password',edit_password.Text);
    end;
    username := edit_username.Text;
    password := edit_password.Text;
  entred := true;
  self.Close;
end;

procedure Tfrm_user.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   if entred = false then Application.Terminate;
end;

procedure Tfrm_user.FormCreate(Sender: TObject);
const
  font = 'BECCA';

begin
  LoadResourceFont('font_BECCA','BECCA');
  self.Font.Name := font;


end;

procedure Tfrm_user.Timer1Timer(Sender: TObject);
begin
edit_username.SetFocus;
timer1.Enabled := False;
end;

end.
