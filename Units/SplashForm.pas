unit SplashForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Vcl.Imaging.GIFImg;

type
  TFormSplash = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected

  public

  end;

var
  FormSplash: TFormSplash;

implementation

{$R *.dfm}


procedure TFormSplash.FormCreate(Sender: TObject);
const
  CORNER_SIZE = 25;
var
  Rgn: HRGN;
begin
Rgn := CreateRoundRectRgn(0, 0, BoundsRect.Right - Left + 1, BoundsRect.Bottom - Top + 1, CORNER_SIZE, CORNER_SIZE);
  if SetWindowRgn(Handle, Rgn, True) = 0 then DeleteObject(Rgn);

end;

procedure TFormSplash.Timer1Timer(Sender: TObject);
begin
if self.AlphaBlendValue >= 255 then
  begin
    timer1.Enabled := false;
    self.Close;
    exit;
  end;
self.AlphaBlendValue := self.AlphaBlendValue + 1;

end;

end.
